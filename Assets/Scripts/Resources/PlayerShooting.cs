﻿using UnityEngine;
using System.Collections;

public class PlayerShooting : MonoBehaviour {

    Animator anim;
    //public GameObject impactsPrefab;
    public LayerMask enemy;


    int currentImpact = 0;
    int maxImpacts = 5;

    bool shooting = false;

    float damage = 25f;

	// Use this for initialization
	void Start () {

        

       //anim = GetComponentInParent<Animator>();

	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetButtonDown("Fire1") && !Input.GetKey(KeyCode.LeftShift)) {
            //muzzleFlash.Play();
            //anim.SetTrigger("Fire");
            shooting = true;
        }
	}

    void FixedUpdate() {
        if (shooting == true)
        {
            shooting = false;

            RaycastHit hit;

            if (Physics.Raycast(transform.position, transform.forward, out hit, Mathf.Infinity))
            {

                Debug.Log("Attempting to Shoot");
                Debug.Log(hit.collider.gameObject.name);
                if (hit.transform.tag == "Player")
                {
                    Debug.Log("Hit tag: Player");
                    hit.transform.GetComponent<PhotonView>().RPC("GetShot", PhotonTargets.All, damage, PhotonNetwork.player.name);
                }
            }


            if (Physics.Raycast(transform.position, transform.forward, out hit, 50f, enemy))
            {
                Debug.Log("hit = " + hit.transform.name);

                Destroy(hit.transform.gameObject);


                //impacts[currentImpact].transform.position = hit.point;
                //impacts[currentImpact].GetComponent<ParticleSystem>().Play();

                if (++currentImpact >= maxImpacts)
                    currentImpact = 0;
            }
        }

    }
}
