﻿using UnityEngine;
using System.Collections;

public class PlayerNetworkMover : Photon.MonoBehaviour {

    public delegate void Respawn(float time);
    public event Respawn RespawnMe;
    public delegate void SendMessage(string message);
    public event SendMessage sendNetworkMessage;

    Vector3 position;
    Quaternion rotation;
    float smoothing = 10f;
    float health = 100f;
    bool aim = false;
    bool sprint = false;
    bool initialLoad = true;

    Animator anim;

    void Start()
    {
        anim = GetComponentInChildren<Animator>();
        if (photonView.isMine)
        {
            GetComponent<Rigidbody>().useGravity = true;
            GetComponent<CharacterController>().enabled = true;
            GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().enabled = true;
            GetComponentInChildren<PlayerShooting>().enabled = true;
            GetComponentInChildren<AudioListener>().enabled = true;
            foreach (Camera cam in GetComponentsInChildren<Camera>())
            {
                cam.enabled = true;
            }

            transform.Find("FirstPersonCharacter/GunCamera/Candy-Cane").gameObject.layer = 8;
        }
        else
        {
            StartCoroutine("UpdateData");
        }
    }


    IEnumerator UpdateData()
    {
        if (initialLoad)
        {
            initialLoad = false;
            transform.position = position;
            transform.rotation = rotation;
        }

        while (true)
        {
            transform.position = Vector3.Lerp(transform.position, position, Time.deltaTime * smoothing);
            transform.rotation = Quaternion.Lerp(transform.rotation, rotation, Time.deltaTime * smoothing);
            anim.SetBool("Aim", aim);
            anim.SetBool("Sprint", sprint);
            yield return null;
        }
    }

    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            stream.SendNext(transform.position);
            stream.SendNext(transform.rotation);
            stream.SendNext(health);
            stream.SendNext(anim.GetBool("Aim"));
            stream.SendNext(anim.GetBool("Sprint"));
        }
        else
        {
            position = (Vector3)stream.ReceiveNext();
            rotation = (Quaternion)stream.ReceiveNext();
            health = (float) stream.ReceiveNext();
            aim = (bool) stream.ReceiveNext();
            sprint = (bool) stream.ReceiveNext();
        }
    }

    [PunRPC]
    public void GetShot(float damage, string enemyName)
    {
        health -= damage;
        if (health <= 0 && photonView.isMine)
        {

            if (sendNetworkMessage != null)
            {
                sendNetworkMessage(PhotonNetwork.player.name + " was killed by" + enemyName);
            }

            if (RespawnMe != null)
            {
                RespawnMe(3f);
            }
            PhotonNetwork.Destroy(gameObject);
        }
    }

}
