﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class NetworkManager : MonoBehaviour {

    [SerializeField] Text connectText;
    [SerializeField] Transform[] spawnPoints;
    [SerializeField] Camera sceneCamera;

    [SerializeField] GameObject serverWindow;
    [SerializeField] InputField userName;
    [SerializeField] InputField roomName;
    [SerializeField] InputField roomList;
    [SerializeField] InputField messageWindow;

    GameObject player;
    Queue<string> messages;
    const int messageCount = 5;
    PhotonView photonView;

	void Start () {
        photonView = GetComponent<PhotonView>();
        messages = new Queue<string>(messageCount);

        PhotonNetwork.logLevel = PhotonLogLevel.Full;
        PhotonNetwork.ConnectUsingSettings("v1");
        StartCoroutine("UpdateConnectionString");
	}
	
	IEnumerator UpdateConnectionString () {
        while (true)
        {
            connectText.text = PhotonNetwork.connectionStateDetailed.ToString();
            yield return null;
        }
	}

    void OnJoinedLobby()
    {
        serverWindow.SetActive(true);
    }

    void OnReceivedRoomListUpdate()
    {
        roomList.text = "";
        RoomInfo[] rooms = PhotonNetwork.GetRoomList(); //only runs if you're in a lobby
        foreach (RoomInfo room in rooms)
        {
            roomList.text += room.name + "\n";
        }
    }

    public void JoinRoom()
    {
        PhotonNetwork.player.name = userName.text;
        RoomOptions ro = new RoomOptions() { isVisible = true, maxPlayers = 10 };
        PhotonNetwork.JoinOrCreateRoom(roomName.text, ro, TypedLobby.Default);
    }

    void OnJoinedRoom()
    {
        serverWindow.SetActive(false);
        StopCoroutine("UpdateConnectionString");
        StartSpawnProcess(0f);
        connectText.text = "";
    }

    void StartSpawnProcess(float respawnTime)
    {
        sceneCamera.enabled = true;
        StartCoroutine("SpawnPlayer", respawnTime);
    }

    IEnumerator SpawnPlayer(float respawnTime)
    {
        yield return new WaitForSeconds(respawnTime);

        int index = Random.Range(0, spawnPoints.Length);
        player = PhotonNetwork.Instantiate("FPSController", spawnPoints[index].position, spawnPoints[index].rotation, 0);
        player.GetComponent<PlayerNetworkMover>().RespawnMe += StartSpawnProcess;
        player.GetComponent<PlayerNetworkMover>().sendNetworkMessage += AddMessage;
        sceneCamera.enabled = false;

        AddMessage("Spawned Player " + PhotonNetwork.player.name);
    }

    void AddMessage(string message)
    {
        photonView.RPC("AddMessage_RPC", PhotonTargets.All, message);
    }

    [PunRPC]
    void AddMessage_RPC(string message)
    {
        messages.Enqueue(message);
        if (messages.Count > messageCount)
        {
            messages.Dequeue();
        }

        messageWindow.text = "";
        foreach (string m in messages)
        {
            messageWindow.text += m + "\n";
        }

    }

}
